﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class MapManager : MonoBehaviour {
	private DominoManager dominoManager;
	public GameObject dominoPrefab;

	void Start () {
		dominoManager = GetComponent<DominoManager>();
		MakeMapFromJSON(Application.loadedLevelName);
	}

	public string GetJSONMap () {
		string jsonMap = "[";
		foreach(GameObject domino in dominoManager.GetDominos()) {
			jsonMap += "{\"x\":\"" + Mathf.Round(domino.transform.position.x * 10.0f) / 10.0f + "\", \"z\":\"" + Mathf.Round(domino.transform.position.z * 10.0f) / 10.0f + "\"},";
		}
		jsonMap = jsonMap.Substring(0, jsonMap.Length - 1);
		jsonMap += "]";
		return jsonMap;
	}

	public void MakeMapFromJSON(string mapName) {
		TextAsset mapJSON = Resources.Load(mapName) as TextAsset;
		IList dominos = (IList)Json.Deserialize(mapJSON.text);
		foreach(IDictionary domino in dominos){
			string x = (string)domino["x"];
			string z = (string)domino["z"];
			Instantiate(dominoPrefab, new Vector3(float.Parse(x), 0.0f, float.Parse(z)), Quaternion.identity);
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DominoManager : MonoBehaviour {
	private List<GameObject> dominos = new List<GameObject>();

	void Start () {
		foreach(var domino in GameObject.FindGameObjectsWithTag("Domino")) {
			dominos.Add(domino);
		}
	}

	public void DestroyAll () {
		foreach(var domino in GameObject.FindGameObjectsWithTag("Domino")) {
			Destroy(domino);
		}
	}

	public List<GameObject> GetDominos() {
		return dominos;
	}

	public int Count () {
		return dominos.Count;
	}

	public int DownCount () {
		int downDominoCount = 0;
		foreach(var domino in dominos) {
			if(domino.GetComponent<Domino>().Down()){
				downDominoCount += 1;
			}
		}
		return downDominoCount;
	}
	
	public bool isStop() {
		foreach(GameObject domino in GameObject.FindGameObjectsWithTag("Domino")) {
			if( domino.GetComponent<Domino>().Speed() > 0.1 ) {
				return false;
			}
		}
		return true;
	}

	public bool isStanding() {
		foreach(GameObject domino in GameObject.FindGameObjectsWithTag("Domino")) {
			if( !domino.GetComponent<Domino>().Down() ) {
				return true;
			}
		}
		return false;
	}
}

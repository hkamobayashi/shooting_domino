﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public GameObject bulletPrefab;
	private bool hitFlag = false;

	public float Speed() {
		return rigidbody.velocity.sqrMagnitude;
	}

	void Update() {
		if (transform.position.y < -1) {
			Destroy(this.gameObject);
		}
	}

	void OnCollisionEnter() {
		if(!hitFlag) {
			GetComponent<AudioSource>().Play();
			hitFlag = true;
		}
	}
}

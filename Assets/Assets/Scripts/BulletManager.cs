﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletManager : MonoBehaviour {
	public GameObject bulletPrefab;
	private BulletManager bulletManager;
	int burst = 5;

	public void Fire (Vector3 target, Vector3 firePosition) {
		if( Burst() > 0 ) {
			GetComponent<AudioSource>().Play();
			float initialVelocity = 1.00f;
			GameObject bullet = Instantiate(bulletPrefab, firePosition, transform.rotation) as GameObject;
			bullet.rigidbody.velocity = (target - transform.position) * initialVelocity;
			CountDown();
		}
	}

	public void DestroyAll () {
		foreach(var domino in GameObject.FindGameObjectsWithTag("Domino")) {
			Destroy(domino);
		}
	}

	public void InitializeCount() {
		burst = 5;
	}

	public void CountDown() {
		if (burst > 0) {
			burst--;
		}
	}

	public int Burst() {
		return burst;
	}

	public bool isStop() {
		foreach(GameObject bullet in GameObject.FindGameObjectsWithTag("Bullet")) {
			if(bullet.GetComponent<Bullet>().Speed() > 20) {
				return false;
			}
		}
		return true;
	}
}

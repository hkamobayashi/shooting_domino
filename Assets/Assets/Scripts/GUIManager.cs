﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {
	[SerializeField] private Texture frame;
	[SerializeField] private Texture gameClear;
	[SerializeField] private Texture gameOver;
	[SerializeField] private Texture gameClearModal;
	[SerializeField] private Texture gameOverModal;
	[SerializeField] private Texture2D retryNormal;
	[SerializeField] private Texture2D retryPressed;
	[SerializeField] private Texture2D bulletTexture;
	[SerializeField] private Texture2D dominoTexture;
	[SerializeField] private Texture2D nextNormal;
	[SerializeField] private Texture2D nextPressed;
	DominoManager dominoManager;
	BulletManager bulletManager;
	float width;
	float height;
	float maxWidth;
	float maxHight;
	GUIStyle style;
	GUIStyle bulletTextureStyle;
	GUIStyle dominoTextureStyle;
	GUIStyle retryButtonStyle;
	GUIStyle nextButtonStyle;
	bool gameClearFlag = false;
	bool gameOverFlag = false;

	void Start() {
		dominoManager = GetComponent<DominoManager>();
		bulletManager = GetComponent<BulletManager>();
		width = Screen.width;
		height = Screen.height;
		maxWidth = 100;
		maxHight = 100;
		style = new GUIStyle();
		style.fontSize = 50;
		style.alignment = TextAnchor.MiddleCenter;
		bulletTextureStyle = new GUIStyle(style);
		bulletTextureStyle.active.background = bulletTexture;
		bulletTextureStyle.normal.textColor = Color.white;
		dominoTextureStyle = new GUIStyle(style);
		dominoTextureStyle.active.background = dominoTexture;
		dominoTextureStyle.normal.textColor = Color.white;
		retryButtonStyle = new GUIStyle();
		retryButtonStyle.normal.background = retryNormal;
		retryButtonStyle.active.background = retryPressed;
		nextButtonStyle = new GUIStyle();
		nextButtonStyle.normal.background = nextNormal;
		nextButtonStyle.active.background = nextPressed;
	}

	void OnGUI () {
		GUI.DrawTexture(
			new Rect(w(5) , h(5), w(30), h(15)), 
			bulletTexture,
			ScaleMode.ScaleToFit
		);
		GUI.Label(
			new Rect(w(10) , h(11), w(20), h(10)), 
			bulletManager.Burst().ToString(),
			bulletTextureStyle
		);
		GUI.DrawTexture(
			new Rect(w(65) , h(5), w(30), h(15)), 
			dominoTexture,
			ScaleMode.ScaleToFit
		);
		GUI.Label(
			new Rect(w(70) , h(11), w(20), h(10)), 
			(dominoManager.Count() - dominoManager.DownCount()).ToString(),
			dominoTextureStyle
		);
		GUI.DrawTexture(
			new Rect(w(0) , h(0), w(100), h(100)), 
			frame
		);
		if(!gameClearFlag || !gameOverFlag) {
			if(dominoManager.Count() == dominoManager.DownCount()) {
				gameClearFlag = true;
			} else if (bulletManager.Burst() == 0 && bulletManager.isStop() && dominoManager.isStop()){
				gameOverFlag = true;
			}
		}
		if(gameClearFlag) {
			GUI.DrawTexture(
				new Rect(w(0) , h(0), w(100), h(100)), 
				gameClear
			);
			GUI.DrawTexture(
				new Rect(w(20) , h(0), w(60), h(70)), 
				gameClearModal,
				ScaleMode.ScaleToFit
			);
			if( GUI.Button( new Rect(w(45), h(55), w(10), h(10)), "", retryButtonStyle ) ) {
				Application.LoadLevel(Application.loadedLevelName);
			}
			if( Application.loadedLevelName == "arrow" ) {
				if( GUI.Button( new Rect(w(45), h(75), w(10), h(10)), "", nextButtonStyle ) ) {
					Application.LoadLevel( "star" );
				}
			}
		}else if(gameOverFlag) {
			GUI.DrawTexture(
				new Rect(w(0) , h(0), w(100), h(100)), 
				gameOver
			);
			GUI.DrawTexture(
				new Rect(w(30) , h(0), w(40), h(70)), 
				gameOverModal,
				ScaleMode.ScaleToFit
			);
			if( GUI.Button( new Rect(w(45), h(65), w(10), h(10)), "", retryButtonStyle ) ) {
				Application.LoadLevel(Application.loadedLevelName);
			}
		}
	} 

	private float w(int i){
		return width * i / maxWidth;
	}

	private float h(int i) {
		return height * i / maxHight;
	}
}

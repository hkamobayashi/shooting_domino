﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {
	float pai = 3.14f;
	float distance = 0;

	void Start () {
		distance = Mathf.Sqrt(Mathf.Pow(transform.position.x, 2) + Mathf.Pow(transform.position.y, 2) + Mathf.Pow(transform.position.z, 2));
	}

	public void Move (Vector2 deltaPosition) {
		Vector3 newPosition = AngleToPosition(
			PositionToAngleXZ(transform.position) - DistanceToAngleXZ(deltaPosition.x),
			Mathf.Min(Mathf.Max (PositionToAngleY(transform.position) + DistanceToAngleY(deltaPosition.y), 0), pai / 2)
		);
		if(newPosition.y < 12f) {
			transform.position = newPosition;
		}
		transform.LookAt(new Vector3(0, 0, 0));
	}

	private Vector3 AngleToPosition(float angleXZ, float angleY){
		return new Vector3(
			distance * Mathf.Sin(angleY) * Mathf.Cos(angleXZ),
			distance * Mathf.Cos(angleY),
			distance * Mathf.Sin(angleY) * Mathf.Sin(angleXZ)
		);
	}

	private float PositionToAngleY(Vector3 position) {
		return Mathf.Atan2(Mathf.Sqrt(Mathf.Pow(position.x, 2) + Mathf.Pow(position.z, 2)), position.y);
	}

	private float PositionToAngleXZ(Vector3 position) {
		return Mathf.Atan2(position.z, position.x);
	}

	private float DistanceToAngleXZ(float distance) {
		return distance / Screen.width * pai;
	}
	
	private float DistanceToAngleY(float distance) {
		return distance / Screen.height * pai;
	}
}

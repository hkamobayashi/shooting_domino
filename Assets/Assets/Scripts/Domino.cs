﻿using UnityEngine;
using System.Collections;

public class Domino : MonoBehaviour {
	private float downArgument;
	
	void Start () {
		downArgument = 40f;
	}

	public bool Down () {
		return Quaternion.Angle(transform.rotation, Quaternion.Euler(0f, 0f, 0f)) > 40f;
	}

	public float Speed() {
		return rigidbody.velocity.sqrMagnitude;
	}
}

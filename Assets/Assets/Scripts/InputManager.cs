﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
	private float dragTime = 0;
	private CameraManager cameraManager;
	private BulletManager bulletManager;
	private Vector2 oldMousePosition = new Vector2(-100f, -100f);

	void Start () {
		cameraManager = GetComponent<CameraManager>();
		bulletManager = GetComponent<BulletManager>();
	}

	void Update () {
#if UNITY_IPHONE || UNITY_ANDROID
		if (Input.touchCount > 0) {
			Vector3 position = Input.GetTouch(0).position;
			bool tapUp = (Input.GetTouch(0).phase == TouchPhase.Ended);
			bool tap = (Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved);
#else
			Vector3 position = Input.mousePosition;
			bool tapUp = Input.GetMouseButtonUp(0);
			bool tap = Input.GetMouseButton(0);
#endif

			if( tapUp ) {
				if( dragTime <= 0.3 ) {
					bulletManager.Fire(
						camera.ScreenToWorldPoint(new Vector3(position.x, position.y + Screen.height / 3, 15f)),
						camera.ScreenToWorldPoint(new Vector3(position.x, position.y, 0.5f))
					);
				}
				dragTime = 0;
			} else if( tap ) {
				dragTime += Time.deltaTime;
				if( oldMousePosition != new Vector2(-100f, -100f) ) {
					Vector2 deltaPosition = new Vector2(position.x, position.y) - oldMousePosition;
					cameraManager.Move(deltaPosition);
				}
				oldMousePosition = new Vector2(position.x, position.y);
			} else {
				oldMousePosition = new Vector2(-100f, -100f);
			}
#if UNITY_IPHONE || UNITY_ANDROID
		}else{
			oldMousePosition = new Vector2(-100f, -100f);
		}
#endif
		//Debug.Log(oldMousePosition);
		//Debug.Log(dragTime);
	}
}
